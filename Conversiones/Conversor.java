class Conversor{
    

    protected double convierte(double valor, String desde, String hacia) {
        double resultado = 0;
        if (desde.equals("km")){
            if(hacia.equals("m")){
                resultado = (valor  / 3.6) ;
            }
            if(hacia.equals("mi")){
                resultado = (valor / 1.61) ;
            }
            if(hacia.equals("ys")){
                resultado = convierte(convierte(valor,"km","mi"),"mi","ys");
            }

        }
        else if(desde.equals("mi")){

            if(hacia.equals("km")){
                resultado = valor * 1.61 ;
            }
            if(hacia.equals("m")){
                resultado = convierte(convierte(valor, "mi", "km"),"km","m") ;
            }
            if(hacia.equals("ys")){
                resultado = valor * 0.4889 ;
            }
        }
        else if(desde.equals("m")){

            if(hacia.equals("km")){
                resultado = (valor * 3.6) ;
            }
            if(hacia.equals("mi")){
                resultado = convierte(convierte(valor,"m","km"),"km","mi") ;
            }
            if(hacia.equals("ys")){
                resultado = convierte(convierte(valor,"m","mi"),"mi","ys");
            }
        }
        else if(desde.equals("ys")){

            if(hacia.equals("km")){
                resultado = convierte(convierte(valor,"ys","mi"),"mi","km") ;
            }
            if(hacia.equals("mi")){
                resultado = valor * 2.04545;
            }
            if(hacia.equals("m")){
                resultado = convierte(convierte(valor,"ys","km"),"km","m");
            }

        }
        
        return resultado;
    }

    // si se considera necesario, crear los métodos adicionales para cada tipo de conversión
    // no es imprescindible, pueden estar todas las conversiones en "convierte"

}