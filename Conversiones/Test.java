import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.ArrayList;

class Test {

    

    private static Scanner scanner = new Scanner( System.in ); // obtener datos por teclado

    private static ArrayList<String> initList(ArrayList<String> a){

        a.add("km");
        a.add("m");
        a.add("mi");
        a.add("ys");

        return a;
    }

    
    private static boolean comprova(String s, ArrayList<String> a){      // comproven que l'input es valid
        for (String it : a) {
            if(s.equals(it)){
                return true;
            }
            
        }
        return false;
    }
    

    public static void main(String[] args) {
        Conversor cnv = new Conversor();
        double valor = 10.0;
        String desde = "km";
        String hacia = "ys";
    
        boolean correct = false;
        ArrayList<String> distancies = new ArrayList<String>();

        distancies = initList(distancies);

        System.out.println("Conversor de km/h, m/s, mi/h y y/s");
        System.out.println("De quina velocitat vols pasar?");
        System.out.print("Escriu ");
        for (String it : distancies) {
            System.out.print(it + " ");
        }
        System.out.println("");
        while(!correct){
            desde = scanner.nextLine();
            if(comprova(desde,distancies)){
                correct = true;
            }
            else{
                System.out.println("T'has equivocat");
                System.out.print("Escriu ");
                for (String it : distancies) {
                    System.out.print(it + " ");
                }
                System.out.println("");
            }
        }
        correct = false;
        distancies.remove(desde);
        System.out.println("A quina velocitat vols pasar?");
        System.out.print("Escriu ");
        for (String it : distancies) {
            System.out.print(it + " ");
        }
        System.out.println("");
        while(!correct){
            hacia = scanner.nextLine();
            if(comprova(hacia,distancies)){
                correct = true;
            }
            else{
                System.out.println("T'has equivocat");
                System.out.print("Escriu ");
                for (String it : distancies) {
                    System.out.print(it + " ");
                }
                System.out.println("");
            }
        }
        correct = false;
        System.out.println("Quina es la velocitat? (numeros)");
        
        while(!correct){   
            try {
                valor = scanner.nextDouble();
                    if(valor >= 0){
                        correct = true;
                    }
                else{
                    System.out.println("Només nombres positius");
                }
            } 
            catch (InputMismatchException e) {
                System.out.println("Posa un numero");
                scanner.nextLine();
            }
            
            
        }
        double resultado = cnv.convierte(valor, desde, hacia);
 
        System.out.printf("%.2f %s equivalen a %.2f %s", valor, desde, resultado, hacia);
        
        
    }
}