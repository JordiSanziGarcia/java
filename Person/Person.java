public class Person {

    String _name;
    int _age;


    Person(){
        _name = "Paco";
        _age = 20;

    }

    Person(String name, int age){

        _name = name;
        _age = age;

    }


    public static void main(String[] args) {

        Person pers1 = new Person("juan",23);
        Person pers2 = new Person();
        System.out.println(pers1._name);
        System.out.println(pers1._age);
        System.out.println(pers2._name);
        System.out.println(pers2._age);
    }


}