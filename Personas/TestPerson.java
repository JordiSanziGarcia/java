import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

public class TestPerson {

    private static ArrayList<Persona> personas = new ArrayList<Persona>();

    private static Scanner scanner = new Scanner( System.in ); // obtener datos por teclado


    /******************************************************
    * 
    *   Funcion menu
    *   Mostramos las diversas opciones del menu
    *
    *******************************************************/
    private static void menu(ArrayList<Persona> p){
        int valor = 0;
        boolean correct;

        do {
            correct = false;
            System.out.println("Que quieres hacer?");
            System.out.println(" ");
            System.out.println("1 ) Modificar datos personas ");
            System.out.println("2 ) Mostrar datos personas ");
            System.out.println("3 ) Comparar personas ");
            System.out.println("4 ) Añadir personas ");
            System.out.println("0 ) Salir ");

            // obetencion de datos
            while(!correct){   
                try {
                    valor = scanner.nextInt();
                        if(valor >= 0  && valor <= 4){
                            correct = true;
                        }
                    else{
                        System.out.println("Escribe un numero de las opciones");
                    }
                } 
                catch (InputMismatchException e) {
                    System.out.println("Escribe el numero de la opciones");
                    scanner.nextLine();
                }
            
            }
            switch(valor) {
                case 1:
                    menuModificar(p);
                    break;
                case 2:
                    PersonaController.MuestraPersonas(p);
                    break;
                case 3:
                    menuCompara(p);
                    break;
                case 4:
                    crearPersona(p);
                    break;
                default:

            }

        } while(valor != 0);
    }

    /******************************************************
    *
    *   Funcion menu Modificar
    *   Escogemos entre personas cual modificamos
    *
    *******************************************************/
    private static void menuCompara(ArrayList<Persona> p){

        int valor1 = 0, valor2 = 0;
        boolean correct ;
        

        correct = false;
        System.out.println("Que quieres comparar ?");
        System.out.println(" ");
        for (Persona it : p) {
            System.out.printf("%d ) ", p.indexOf(it)+1);
            PersonaController.MuestraPersona(it);
        }
        System.out.println("0 ) Salir ");   
        
        System.out.println("Escribe el numero de una persona a comparar");
        // obetencion de primer valor
        while(!correct){   
            try {
                valor1 = scanner.nextInt();
                    if(valor1 > 0  && valor1 <= p.size()){
                        correct = true;
                    }
                else{
                    System.out.println("Escribe un numero de las opciones");
                }
            } 
            catch (InputMismatchException e) {
                System.out.println("Escribe el numero de la opciones");
                scanner.nextLine();
            }
        
        }
        correct = false;

        System.out.println("Escribe el numero de una persona a comparar");
        // obtenemos segundo valor
        while(!correct){   
            try {
                valor2 = scanner.nextInt();
                    if(valor2 > 0  && valor2 <= p.size() && valor2 != valor1 ){
                        correct = true;
                    }
                else{
                    System.out.println("Escribe un numero de las opciones");
                }
            } 
            catch (InputMismatchException e) {
                System.out.println("Escribe el numero de la opciones");
                scanner.nextLine();
            }
        
        }
        
        PersonaController.ComparaPersona(p.get(valor1-1), p.get(valor2-1));               

    }
    

    /******************************************************
    *
    *   Funcion menu Modificar
    *   Escogemos entre personas cual modificamos
    *
    *******************************************************/
    private static void menuModificar(ArrayList<Persona> p){

        int valor = 0;
        boolean correct ;
        
        do {
            correct = false;
            System.out.println("Que quieres hacer ?");
            System.out.println(" ");
            for (Persona it : p) {
                System.out.printf("%d ) Modificar ", p.indexOf(it)+1);
                PersonaController.MuestraPersona(it);
            }
            System.out.println("0 ) Salir ");   
            

            // obetencion de datos
            while(!correct){   
                try {
                    valor = scanner.nextInt();
                        if(valor >= 0  && valor <= p.size()){
                            correct = true;
                        }
                    else{
                        System.out.println("Escribe un numero de las opciones");
                    }
                } 
                catch (InputMismatchException e) {
                    System.out.println("Escribe el numero de la opciones");
                    scanner.nextLine();
                }
            
            }
            if(valor != 0){

                menuModificarPersona(p.get(valor-1));    

            }
            
        } while(valor != 0);
    }

    /******************************************************
    *   Funcion menu modificar persona 
    *   mostramos un menu y permitimos la modificacion de datos
    *
    *******************************************************/

    private static void menuModificarPersona(Persona p){
        int valor = 0;
        boolean correct;

        do{
            correct = false;
            System.out.println("Que quieres hacer ?");
            System.out.println(" ");
            PersonaController.MuestraPersona(p); 
            System.out.println("1 ) Modificar nombre");
            System.out.println("2 ) Modificar edad");
            System.out.println("3 ) Modificar email");
            System.out.println("4 ) Modificar id");
            System.out.println("0 ) Salir ");   
              
            

            // obetencion de datos
            while(!correct){   
                try {
                    valor = scanner.nextInt();
                        if(valor >= 0  && valor <= 4){
                            correct = true;
                        }
                    else{
                        System.out.println("Escribe un numero de las opciones");
                    }
                } 
                catch (InputMismatchException e) {
                    System.out.println("Escribe el numero de la opciones");
                    scanner.nextLine();
                }
            
            }
            switch(valor) {
                case 1:
                    modNom(p);
                    break;
                case 2:
                    modEdat(p);
                    break;
                case 3:
                    modEmail(p);
                    break;
                case 4:
                    modId(p);
                    break;
                default:

            }
        } while(valor != 0);
    }

    private static void modNom(Persona p){


        String nom = " ";
        boolean correct = false;

        while(!correct){   
        

            nom = scanner.nextLine();

            Pattern r = Pattern.compile(".+");
 
            Matcher m = r.matcher(nom);
            if( m.find()){
                    correct = true;
            }
            else{
                System.out.println("Introduce un nombre");
            }
            
           
        }
        p.setNom(nom);

    }

    private static void modEdat(Persona p){
        int valor = 0;
        boolean correct = false;
        System.out.println("Introduce la edad");
        while(!correct){   
            try {
                valor = scanner.nextInt();
                    if(valor >= 0 && valor < 150 ){
                        correct = true;
                    }
                else{
                    System.out.println("Solo numeros positivos y menores de 150");
                }
            } 
            catch (InputMismatchException e) {
                System.out.println("Posa un numero");
                scanner.nextLine();
            }
        }
        p.setEdat(valor);

    }

    private static void modEmail(Persona p){

    
        String mail = " ";
        boolean correct = false;

        System.out.println("Introduce el email");
        while(!correct){   
        

            mail = scanner.nextLine();

            Pattern r = Pattern.compile(".+@.+\\..+");          // te controla que tenga @ y . algo, dedidcado a Eugenio.
 
            Matcher m = r.matcher(mail);
                if( m.find()){
                    correct = true;
                }
            else{
                System.out.println("Pon un correo xxxx@xxxx.xxx ");
            }
            
           
        }
        p.setEmail(mail);

    }

    private static void modId(Persona p){
        int valor = 0;
        boolean correct = false;

        System.out.println("Introduce el Id");
        while(!correct){   
            try {
                valor = scanner.nextInt();
                    if(valor >= 0 ){
                        correct = true;
                    }
                else{
                    System.out.println("Solo numeros positivos ");
                }
            } 
            catch (InputMismatchException e) {
                System.out.println("Posa un numero");
                scanner.nextLine();
            }
        }
        p.setId(valor);

    }

    private static void crearPersona(ArrayList<Persona> p){
        Persona pers = new Persona();
        modNom(pers);
        modId(pers);
        modEmail(pers);
        modEdat(pers);
        p.add(pers);

    }

    

    /******************************************************
    *   Funcion principal 
    *   Asiganmos variables por defecto y luego mostramos un menu para modificar, mostrar, comparar o salir.
    *
    *******************************************************/
    public static void main(String[] args) {
        
        int valor = 0;
        boolean correct = false;



        

        // valores por defecto
        Persona person1 = new Persona(25, "Eugenio", "casaDeCampo@gmail.com", 23);  // creamos por constructor
        personas.add(person1);


        Persona person2 = new Persona();        // creamos con constructor vacio y le damos valores
        person2.setId(26);
        person2.setNom("Paco");
        person2.setEdat(34);
        person2.setEmail("chocolate@gmail.com");
        personas.add(person2);


        System.out.println("Persona de prueba");

        // mostramos persona

        PersonaController.MuestraPersonas(personas);
    

        System.out.println("");

        // comparamos persona
        PersonaController.ComparaPersona(person1, person2);
        PersonaController.ComparaPersona(person2, person1);
        System.out.println("");

        // escribimos el menu
        do {

            menu(personas);
            
        } while(valor != 0);
    
        
    }
}


