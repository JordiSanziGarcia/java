import java.util.ArrayList;

public class PersonaController {

    // clase para comprobar


    // metodo mostrar objeto Persona
    protected static void MuestraPersona(Persona pers){
        System.out.printf("%s (id : %d) tiene %d anyos y su email es %s \n", pers.getNom(), pers.getId(), pers.getEdat(), pers.getEmail());
        
    }

    // metodo mostrar objeto Persona
    protected static void MuestraPersonas(ArrayList<Persona> p){
        for (Persona it : p) {
            PersonaController.MuestraPersona(it);
        }
    }

    // metodo para comparar 2 objetos persona   
    protected static void ComparaPersona(Persona pers1, Persona pers2){
        if(pers1.getEdat() < pers2.getEdat()){      // menor
            System.out.printf("%s (%d) es menor que %s (%d) \n", pers1.getNom(), pers1.getEdat(), pers2.getNom(), pers2.getEdat() ); 
        }
        else if(pers1.getEdat() > pers2.getEdat()){     // mayor
            System.out.printf("%s (%d) es mayor que %s (%d) \n", pers1.getNom(), pers1.getEdat(), pers2.getNom(), pers2.getEdat() ); 
        }
        else {      // igual
            System.out.printf("%s (%d) y %s (%d)  tienen la misma edad \n", pers1.getNom(), pers1.getEdat(), pers2.getNom(), pers2.getEdat() ); 
        }
    }


}