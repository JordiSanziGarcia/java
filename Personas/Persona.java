public class Persona {

    // variables privadas de la clase
    private  String _nom;
    private int _edat;
    private String _email;
    private  int _id;


    // constructor vacio
    Persona(){

    }

    //constructor con parametros
    Persona(int id, String nom, String email,int edat){
        this._nom = nom;
        this._edat = edat;
        this._email = email;
        this._id = id;

    }

    // get / set Id
    protected int getId(){
        return this._id;
    }
    protected void setId(int id){
        this._id = id;
    }

    // get / set Nom
    protected String getNom(){
        return this._nom;
    }
    protected void setNom(String nom){
        this._nom = nom;
    }

    // get / set Edat
    protected int getEdat(){
        return this._edat;
    }
    protected void setEdat(int edat){
        this._edat = edat;
    }

    // get / set Email
    protected String getEmail(){
        return this._email;
    }
    protected void setEmail(String email){
        this._email = email;
    }



}