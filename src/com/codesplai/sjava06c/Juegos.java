package com.codesplai.sjava06c; 
import java.util.Random; 
import java.util.Scanner;
import java.util.InputMismatchException;

public class Juegos { 

    public static Jugador jugador1; 
    public static Jugador jugador2; 
    public static int partidas; 
    public static Scanner keyboard = new Scanner(System.in); 



    /** * pide nombre de jugador, 1 o 2 
    * crea objeto "Jugador" y lo asigna a jugador1 o jugador2 */ 
    public static void pideJugador(int numJugador){ 
    
        //pedimos nombre de jugador 
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador); 
        System.out.println(pregunta); 
        String nombre = keyboard.next(); 
        Jugador j = new Jugador(nombre); 

        if (numJugador == 1) { 
            Juegos.jugador1 = j; 
        }else{ 
            Juegos.jugador2 = j; 
        } 
    } 

    /** * muestra menu de juegos y pide opción */ 
    public static void menu(){ 
        System.out.println("*******************"); 
        System.out.println("JUEGOS DISPONIBLES:"); 
        System.out.println("*******************"); 
        System.out.println("1: Cara o Cruz"); 
        System.out.println("2: Piedra, Papel o Tijeras"); 
        System.out.println("3: "); 
        System.out.println("*******************"); 
        
        //pedimos opcion de juego, comprobando validez 
        int opcion = 0; 

        do
        { 
            System.out.print("Introduce juego: "); 
            opcion = keyboard.nextInt(); 

        } while (opcion<1 || opcion>2); 

        switch (opcion) { 
            case 1: 
                CaraCruz.jugar(); 
                break; 
            case 2:
                PiedraPapelTijera.jugar();
            case 3:
                Ruleta.jugar();
            default: 
                break; 
        } 
    } 

    
}