package com.codesplai.sjava06c;
import java.util.Random; 
import java.util.Scanner;
import java.util.InputMismatchException;

public class Ruleta { 
    
    public static Scanner keyboard = new Scanner(System.in); 
    
    // Constante para Ruleta 
    private final static byte ROJO = 0;
    private final static byte NEGRO = 1;
    
    private static int saldo = 100;
    private static byte[] apuesta = {0,0,0};
    private static int[] dinero = {0,0,0};
    
    public static void main(String[] args) { 

        Juegos.menu(); 
    } 
    
    
    // creamos un metodo para la validacion de datos de entrada
    protected static void jugar(){ 

        
        
        menuRuleta();
        // obtencion de datos
        // jugar ?
    } 


    // menu ruleta
    private static void menuRuleta(){
        byte valor = 0;
        boolean correct;



        do {
            correct = false;
            System.out.println("Que quieres hacer?");
            System.out.println(" ");
            System.out.println("1 ) Apuesta Rojo/Negro ");
            System.out.println("2 ) Apuesta Par/Impar ");
            System.out.println("3 ) Apuesta Falta/Pasa ");
            System.out.println("0 ) Salir ");

            // obetencion de datos
            while(!correct){   
                try {
                    valor = keyboard.nextByte();
                        if(valor >= 0  && valor <= 3){
                            correct = true;
                        }
                    else{
                        System.out.println("Escribe un numero de las opciones");
                    }
                } 
                catch (InputMismatchException e) {
                    System.out.println("Escribe el numero de la opciones");
                    keyboard.nextLine();
                }
            
            }
            switch(valor) {
                case 1:
                    rojoNegro();
                    break;
                case 2:
                    parImpar();
                    break;
                case 3:
                    faltaPasa();
                    break;
                default:

            }

        } while(valor != 0);
    }

    
    // funcion para apostar en rojo o negro
    private static void rojoNegro(){
        byte valor = 0;
        boolean correct;

        do {
            correct = false;
            System.out.println("Que quieres hacer?");
            System.out.println(" ");
            System.out.println("1 ) Apuesta Rojo ");
            System.out.println("2 ) Apuesta Negro ");
            System.out.println("0 ) Salir ");

            // obetencion de datos
            while(!correct){   
                try {
                    valor = keyboard.nextByte();
                        if(valor >= 0  && valor <= 2){
                            correct = true;
                        }
                    else{
                        System.out.println("Escribe un numero de las opciones");
                    }
                } 
                catch (InputMismatchException e) {
                    System.out.println("Escribe el numero de la opciones");
                    keyboard.nextLine();
                }
            
            }
            switch(valor) {
                case 1:
                    apuesta[0] = 1;
                    dinero[0] = 0; // reiniciamos el dinero de la apuesta 
                    dinero[0] = introducirApuesta();
                    break;
                case 2:
                    apuesta[0] = 2;
                    dinero[0] = 0; // reiniciamos el dinero de la apuesta 
                    dinero[0] = introducirApuesta();
                    break;
                default:

            }

        } while(valor != 0);
    }
    
    
    // funcion para par o impar
    private static void parImpar(){
        byte valor = 0;
        boolean correct;

        do {
            correct = false;
            System.out.println("Que quieres hacer?");
            System.out.println(" ");
            System.out.println("1 ) Apuesta Par ");
            System.out.println("2 ) Apuesta Impar ");
            System.out.println("0 ) Salir ");

            // obetencion de datos
            while(!correct){   
                try {
                    valor = keyboard.nextByte();
                        if(valor >= 0  && valor <= 2){
                            correct = true;
                        }
                    else{
                        System.out.println("Escribe un numero de las opciones");
                    }
                } 
                catch (InputMismatchException e) {
                    System.out.println("Escribe el numero de la opciones");
                    keyboard.nextLine();
                }
            
            }
            
            // guardamos en la psicion de par o impar la apuesta
            switch(valor) {
                case 1:
                    apuesta[1] = 1; 
                    dinero[1] = 0; // reiniciamos el dinero de la apuesta 
                    dinero[1] = introducirApuesta();
                    break;
                case 2:
                    apuesta[1] = 2;
                    dinero[1] = 0; // reiniciamos el dinero de la apuesta 
                    dinero[1] = introducirApuesta();
                    break;
                default:

            }

        } while(valor != 0);
    }

    
    // funcion para falta o pasa
    private static void faltaPasa(){
        byte valor = 0;
        boolean correct;

        do {
            correct = false;
            System.out.println("Que quieres hacer?");
            System.out.println(" ");
            System.out.println("1 ) Apuesta Par ");
            System.out.println("2 ) Apuesta Impar ");
            System.out.println("0 ) Salir ");

            // obetencion de datos
            while(!correct){   
                try {
                    valor = keyboard.nextByte();
                        if(valor >= 0  && valor <= 2){
                            correct = true;
                        }
                    else{
                        System.out.println("Escribe un numero de las opciones");
                    }
                } 
                catch (InputMismatchException e) {
                    System.out.println("Escribe el numero de la opciones");
                    keyboard.nextLine();
                }
            
            }
            
            // guardamos en la psicion de par o impar la apuesta
            switch(valor) {
                case 1:
                    apuesta[2] = 1; 
                    dinero[2] = 0; // reiniciamos el dinero de la apuesta 
                    dinero[2] = introducirApuesta();
                    break;
                case 2:
                    apuesta[2] = 2;
                    dinero[2] = 0;   // reiniciamos el dinero de la apuesta 
                    dinero[2] = introducirApuesta();
                    break;
                default:

            }

        } while(valor != 0);
    }

    
    // funcion introducir apuesta
    private static int introducirApuesta(){
        
        int valor = 0;
        boolean correct = false;
        int apuestas = 0;

        for (int it : dinero) {
            apuestas += dinero[it];
        }

        System.out.println("Escribe cuanto quieres apostar");        
        while(!correct){   
            try {
                valor = keyboard.nextByte();
                    if(valor >= 0  && valor <= saldo-apuestas){
                        correct = true;
                    }
                else{
                    System.out.println("Saldo inferior");
                }
            } 
            catch (InputMismatchException e) {
                System.out.println("Escribe un numero valido");
                keyboard.nextLine();
            }
        
        }
        return valor;
    }
} 


