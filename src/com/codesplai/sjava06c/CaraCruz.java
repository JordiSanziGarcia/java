package com.codesplai.sjava06c; 
import java.util.Random; 
import java.util.Scanner;
import java.util.InputMismatchException;

public class CaraCruz { 

    public static Scanner keyboard = new Scanner(System.in); 

/** * juego caraCruz  */ 
    
    protected static void jugar(){ 
        Juegos.pideJugador(1); 
        //solo interviene un jugador en este juego 
        
        Juegos.partidas = 5; 
        //partidas por defecto en este juego 

        System.out.println(); 
        System.out.println("CARA O CRUZ:"); 
        System.out.println("************"); 
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre); 
        System.out.println(); 
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas); 
        System.out.println(); 

        Random rnd = new Random(); //bucle de partidas 
        for(int i=0; i<Juegos.partidas; i++){ 
            System.out.print("Cara (C) o cruz(X) ? ");  //pedimos apuesta aunque no se utiliza 
            String apuesta = keyboard.next();           // sea cual sea la probabilidad es un 50%... 
            
            // obtenemos un boolean aleatorio 
            boolean ganador = rnd.nextBoolean(); 
            if (ganador) { 

                System.out.println(" Has acertado!"); 
                Juegos.jugador1.ganadas++; 
            } else { 
                System.out.println(" Lo siento..."); 
            } 
            
            Juegos.jugador1.partidas++; 
        } 
        
        //creamos string con el resumen final de juego y lo mostramos 
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas); 
        System.out.println(resumen); 
    } 
}